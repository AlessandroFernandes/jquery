var frase = $(".frase").text();
var numFrase = frase.split(" ").length;
var contadorFrase = $("#contador-frase").text(numFrase);
var campo = $(".campo-digitacao");
var resete = $("#resetar");
var tempoInicial = $("#segundos").text();

$(function () {
    contador();
    gameover();
    resetar();
})

function contador() {
    campo.on("input", function () {
        var conteudo = campo.val();
        var letras = conteudo.length;
        var contador = $("#contador-letras").text(letras);

        var palavras = conteudo.split(/\s+/).length - 1;
        var contadorPalavras = $("#contador-palavras").text(palavras);

    })
}


function gameover() {
    campo.one("focus", function () {
        var segundos = $("#segundos").text();
        var cronometro = setInterval(function () {
            segundos--;
            $("#segundos").text(segundos);
            if (segundos < 1) {
                clearInterval(cronometro);
                campo.attr("disabled", true);
            }

        }, 1000);
    })
}

function resetar() {
    resete.click(function () {
        $("#contador-palavras").text("0");
        $("#contador-letras").text("0")
        $(".campo-digitacao").val("");
        $(".campo-digitacao").attr("disabled", false);
        $("#segundos").text(tempoInicial);
        gameover();
    })
}




